package com.changer.ocm.app;

import javax.servlet.ServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class OpenmapSleuthServiceApplication {
	
	private static final Logger log = LoggerFactory.getLogger(OpenmapSleuthServiceApplication.class);

	
	public static void main(String[] args) {
		SpringApplication.run(OpenmapSleuthServiceApplication.class, args);
	}

	@RequestMapping("/abc")
	public String test(ServletRequest request) throws Exception {
		log.info("===========test.test.test");
		String test = request.getParameter("isTest");
		if(null != test && "true".equalsIgnoreCase(test)) {
			try {
			
			throw new Exception("test error");
			}catch (Exception e) {
				log.error("===========test.test.test");
			}
		}
		return "test.test.test";
	}
}
