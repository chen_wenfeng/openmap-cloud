package com.changer.openmap.app.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class OpenmapConfigServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(OpenmapConfigServiceApplication.class, args);
	}

}
