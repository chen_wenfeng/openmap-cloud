package com.changer.openmap.boot.admin.utils;

import java.io.IOException;
import java.net.*;

public class PTUtil {
	/***
	 * ping操作
	 * 
	 * @param hostname
	 * @param timeout  in milliseconds
	 * @return
	 */
	public static PTResult pingResult(String hostname, Integer timeout) {
		PTResult jsonResult = new PTResult();
		try {
			InetAddress address = InetAddress.getByName(hostname);
			boolean flag = address.isReachable(timeout);
			if (flag) {
				jsonResult.setMessage("ping: the address is reachable.");
			} else {
				jsonResult.setCode(PTConsts.CheckStatus.OFFLINE);
				jsonResult.setMessage("ping: the address is unreachable.");
			}
		} catch (UnknownHostException e) {
			jsonResult.setCode(PTConsts.CheckStatus.UNKNOWN);
			jsonResult.setMessage("ping: UnknownHostException:" + e.getMessage());
		} catch (IOException e) {
			jsonResult.setCode(PTConsts.CheckStatus.ABNORMAL);
			jsonResult.setMessage("ping: IOException:" + e.getMessage());
		}
		return jsonResult;
	}
	
	/***
	 * telnet 操作
	 * 
	 * @param hostname
	 * @param timeout  in milliseconds
	 * @return
	 */
	public static PTResult telnetResult(String hostname, Integer port, Integer timeout) {
		PTResult jsonResult = new PTResult();
		try {
			Socket server = new Socket();
			InetSocketAddress address = new InetSocketAddress(hostname, port);
			server.connect(address, timeout);
			server.close();
			jsonResult.setMessage("telnet: success!");
		} catch (UnknownHostException e) {
			jsonResult.setCode(PTConsts.CheckStatus.OFFLINE);
			jsonResult.setMessage("telnet: UnknownHostException:" + e.getMessage());
		} catch (IOException e) {
			jsonResult.setCode(PTConsts.CheckStatus.OFFLINE);
			jsonResult.setMessage("telnet: IOException:" + e.getMessage());
		}
		return jsonResult;
	}
}