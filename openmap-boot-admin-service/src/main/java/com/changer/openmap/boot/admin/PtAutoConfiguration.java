package com.changer.openmap.boot.admin;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("openmap.boot.admin.pt")
public class PtAutoConfiguration {
	/**
	 * 主机ip
	 */
	private String host = "127.0.0.1";
	/**
	 * 是否启检测
	 */
	private boolean checkEnable = true;

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public boolean isCheckEnable() {
		return checkEnable;
	}

	public void setCheckEnable(boolean checkEnable) {
		this.checkEnable = checkEnable;
	}


}
