package com.changer.openmap.boot.admin.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.changer.openmap.boot.admin.utils.PTConsts;
import com.changer.openmap.boot.admin.utils.PTConsts.CheckStatus;

public class StatusInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private PTConsts.CheckStatus status = PTConsts.CheckStatus.NORMAL;
    private final Map<String, Object> details = new HashMap<>();
    
    
	public StatusInfo() {
		super();
	}
	public StatusInfo(CheckStatus status) {
		super();
		this.status = status;
	}
	
	
	public PTConsts.CheckStatus getStatus() {
		return status;
	}
	public void setStatus(PTConsts.CheckStatus status) {
		this.status = status;
	}
	public Map<String, Object> getDetails() {
		return details;
	}

}
