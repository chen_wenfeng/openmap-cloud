package com.changer.openmap.boot.admin.service;

public interface ICheckService {
	boolean checkTelnet(String host, int port);
	boolean checkPing(String host);
}
