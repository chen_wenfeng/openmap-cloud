package com.changer.openmap.boot.admin.notify;

import static java.util.Collections.singleton;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.changer.openmap.boot.admin.config.PTCheckConfig;
import com.changer.openmap.boot.admin.events.OcmInstanceEvent;

import reactor.core.publisher.Mono;

public class OcmEMailNotifier extends OcmAbstractEventNotifier {
    private final JavaMailSender mailSender;
    private final TemplateEngine templateEngine;

    /**
     * recipients of the mail
     */
    private String[] to = {"root@localhost"};

    /**
     * cc-recipients of the mail
     */
    private String[] cc = {};

    /**
     * sender of the change
     */
    private String from = "Spring Boot Admin <noreply@localhost>";

    /**
     * Additional properties to be set for the template
     */
    private Map<String, Object> additionalProperties = new HashMap<>();

    /**
     * Base-URL used for hyperlinks in mail
     */
    private String baseUrl;

    /**
     * Thymleaf template for mail
     */
    private String template = "classpath:/mail/ocm-status-changed.html";

    public OcmEMailNotifier(JavaMailSender mailSender, TemplateEngine templateEngine) {
        super();
        this.mailSender = mailSender;
        this.templateEngine = templateEngine;
    }

    @Override
    protected Mono<Void> doNotify(OcmInstanceEvent event) {
        return Mono.fromRunnable(() -> {
            Context ctx = new Context();
            ctx.setVariables(additionalProperties);
            ctx.setVariable("baseUrl", this.baseUrl);
            ctx.setVariable("event", event);
            ctx.setVariable("instance", event.getInstance());
            ctx.setVariable("lastStatus", getLastStatus(event.getInstance()));

            try {
                MimeMessage mimeMessage = mailSender.createMimeMessage();
                MimeMessageHelper message = new MimeMessageHelper(mimeMessage, StandardCharsets.UTF_8.name());
                message.setText(getBody(ctx).replaceAll("\\s+\\n", "\n"), true);
                message.setSubject(getSubject(ctx));
                message.setTo(this.to);
                message.setCc(this.cc);
                message.setFrom(this.from);
                mailSender.send(mimeMessage);
            } catch (MessagingException ex) {
                throw new RuntimeException("Error sending mail notification", ex);
            }
        });
    }
    
    protected final String getLastStatus(PTCheckConfig instance) {
        return instance.getLastStatus().getDesc();
    }

    protected String getBody(Context ctx) {
        return templateEngine.process(this.template, ctx);
    }

    protected String getSubject(Context ctx) {
        return templateEngine.process(this.template, singleton("subject"), ctx).trim();
    }

    public void setTo(String[] to) {
        this.to = Arrays.copyOf(to, to.length);
    }

    public String[] getTo() {
        return Arrays.copyOf(to, to.length);
    }

    public void setCc(String[] cc) {
        this.cc = Arrays.copyOf(cc, cc.length);
    }

    public String[] getCc() {
        return Arrays.copyOf(cc, cc.length);
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getFrom() {
        return from;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

}
