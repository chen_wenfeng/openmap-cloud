package com.changer.openmap.boot.admin.notify;

import com.changer.openmap.boot.admin.events.OcmInstanceEvent;

import reactor.core.publisher.Mono;

public abstract class OcmAbstractEventNotifier implements OcmNotifier {
    private boolean enabled = true;

    @Override
    public Mono<Void> notify(OcmInstanceEvent event){
        if (!enabled) {
            return Mono.empty();
        }
        
        return doNotify(event);
    }
    
    protected abstract Mono<Void> doNotify(OcmInstanceEvent event);

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
