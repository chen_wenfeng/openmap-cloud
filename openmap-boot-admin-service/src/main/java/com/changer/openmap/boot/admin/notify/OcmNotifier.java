package com.changer.openmap.boot.admin.notify;

import com.changer.openmap.boot.admin.events.OcmInstanceEvent;

import reactor.core.publisher.Mono;

public interface OcmNotifier {
	   Mono<Void> notify(OcmInstanceEvent event);

}
