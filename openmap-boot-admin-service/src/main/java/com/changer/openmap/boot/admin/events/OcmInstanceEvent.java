package com.changer.openmap.boot.admin.events;

import java.io.Serializable;

import com.changer.openmap.boot.admin.config.PTCheckConfig;
import com.changer.openmap.boot.admin.model.StatusInfo;

public class OcmInstanceEvent  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private PTCheckConfig instance;
	private StatusInfo statusInfo = new StatusInfo();

	public OcmInstanceEvent() {
		super();
	}
	
	public OcmInstanceEvent(PTCheckConfig instance) {
		super();
		this.instance = instance;
	}
	public OcmInstanceEvent(PTCheckConfig instance, StatusInfo statusInfo) {
		super();
		this.instance = instance;
		this.statusInfo = statusInfo;
	}

	public PTCheckConfig getInstance() {
		return instance;
	}

	public void setInstance(PTCheckConfig instance) {
		this.instance = instance;
	}

	public StatusInfo getStatusInfo() {
		return statusInfo;
	}

	public void setStatusInfo(StatusInfo statusInfo) {
		this.statusInfo = statusInfo;
	}
}
