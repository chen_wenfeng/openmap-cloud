package com.changer.openmap.boot.admin.config;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.changer.openmap.boot.admin.utils.PTConsts;
import com.changer.openmap.boot.admin.utils.PTConsts.CheckType;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class PTCheckConfig {
	private PTConsts.CheckType type = CheckType.TELNET;
	private String host;
	private int port = 0;
	private int timeout = 3000;
	//启动时间(只获得时间部份)
	private String startTime;
	//结束时间
	private String endTime;
	//间隔
	private long interval;
	//失败最大次数
	private int failMaxCount = 3;
	//附加属性
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	//服务名
	private String serviceName;
	//服务描述
	private String serviceDesc;
	

	//临时最后时间
	@JsonIgnore
	private Date tempLastTime = new Date();
	@JsonIgnore
	private String fileName;
	@JsonIgnore
	private long tempRealSendInterval;
	@JsonIgnore
	private int tempFailCount = 0;
	@JsonIgnore
	private PTConsts.CheckStatus status = PTConsts.CheckStatus.NORMAL;
	@JsonIgnore
	private PTConsts.CheckStatus lastStatus = PTConsts.CheckStatus.NORMAL;
	

	
	public PTCheckConfig() {
		super();
	}


	public PTCheckConfig(String host, Integer port, int timeout) {
		super();
		this.host = host;
		this.port = port;
		this.timeout = timeout;
	}


	public String getServiceUrl() {
		if(PTConsts.CheckType.PING.equals(this.type)) {
			return host;
		}else {
			return host + ":" + port;
		}
	}
	
	public PTConsts.CheckType getType() {
		return type;
	}


	public void setType(PTConsts.CheckType type) {
		this.type = type;
	}


	public String getHost() {
		return host;
	}


	public void setHost(String host) {
		this.host = host;
	}


	public int getPort() {
		return port;
	}


	public void setPort(int port) {
		this.port = port;
	}


	public int getTimeout() {
		return timeout;
	}


	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}


	public String getStartTime() {
		return startTime;
	}


	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}


	public String getEndTime() {
		return endTime;
	}


	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}


	public long getInterval() {
		return interval;
	}


	public void setInterval(long interval) {
		this.interval = interval;
	}


	public Date getTempLastTime() {
		return tempLastTime;
	}


	public void setTempLastTime(Date tempLastTime) {
		this.tempLastTime = tempLastTime;
	}


	public String getFileName() {
		return fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public long getTempRealSendInterval() {
		return tempRealSendInterval;
	}


	public void setTempRealSendInterval(long tempRealSendInterval) {
		this.tempRealSendInterval = tempRealSendInterval;
	}


	@Override
	public String toString() {
		return "PTCheckConfig [type=" + type + ", host=" + host + ", port=" + port + ", timeout=" + timeout
				+ ", startTime=" + startTime + ", endTime=" + endTime + ", interval=" + interval + ", fileName="
				+ fileName + "]";
	}


	public int getFailMaxCount() {
		return failMaxCount;
	}


	public void setFailMaxCount(int failMaxCount) {
		this.failMaxCount = failMaxCount;
	}


	public int getTempFailCount() {
		return tempFailCount;
	}


	public void setTempFailCount(int tempFailCount) {
		this.tempFailCount = tempFailCount;
	}


	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}


	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}


	public PTConsts.CheckStatus getLastStatus() {
		return lastStatus;
	}


	public void setLastStatus(PTConsts.CheckStatus lastStatus) {
		this.lastStatus = lastStatus;
	}


	public String getServiceName() {
		return serviceName;
	}


	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}


	public String getServiceDesc() {
		return serviceDesc;
	}


	public void setServiceDesc(String serviceDesc) {
		this.serviceDesc = serviceDesc;
	}


	public PTConsts.CheckStatus getStatus() {
		return status;
	}


	public void setStatus(PTConsts.CheckStatus status) {
		this.status = status;
	}
	
	
}
