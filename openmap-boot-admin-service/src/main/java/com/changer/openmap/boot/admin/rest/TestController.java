package com.changer.openmap.boot.admin.rest;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.changer.openmap.boot.admin.config.PTCheckConfig;
import com.changer.openmap.boot.admin.events.OcmInstanceEvent;
import com.changer.openmap.boot.admin.notify.OcmEMailNotifier;

import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@RestController
public class TestController {
	private static final Logger s_logger = LoggerFactory.getLogger(TestController.class);
	@Autowired
	private OcmEMailNotifier emailNotifier;

	
	@RequestMapping(value = "/test/send/mail", method = RequestMethod.GET)
	@ApiOperation(value = "测试发送mail")
	@ApiImplicitParams({
//		@ApiImplicitParam(name = "objectId", value = "查询对象id ", required = true, dataType = "long", paramType = "path"),
//		@ApiImplicitParam(name = "startTime", value = "开始时间", required = false, dataType = "date", paramType = "query"),
//		@ApiImplicitParam(name = "endTime", value = "结束时间", required = false, dataType = "date", paramType = "query"),
//		@ApiImplicitParam(name = "isContainTime", value = "包含时间", required = false, dataType = "boolean", paramType = "query"),
	})
	public String doQuery(@ApiIgnore ServletRequest request, @ApiIgnore ServletResponse response) {
		try {
			PTCheckConfig pt = new PTCheckConfig("test", 0, 1000);
			OcmInstanceEvent event = new OcmInstanceEvent(pt);
			emailNotifier.notify(event).subscribe();
			return "ok";
		} catch (Exception e) {
			s_logger.error(e.getMessage(), e);
			return e.getMessage();
		}

	}

}
