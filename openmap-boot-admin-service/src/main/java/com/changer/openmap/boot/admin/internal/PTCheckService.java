package com.changer.openmap.boot.admin.internal;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import com.changer.openmap.boot.admin.PtAutoConfiguration;
import com.changer.openmap.boot.admin.config.PTCheckConfig;
import com.changer.openmap.boot.admin.events.OcmInstanceEvent;
import com.changer.openmap.boot.admin.model.StatusInfo;
import com.changer.openmap.boot.admin.notify.OcmAbstractEventNotifier;
import com.changer.openmap.boot.admin.service.ICheckService;
import com.changer.openmap.boot.admin.utils.DataJsonParser;
import com.changer.openmap.boot.admin.utils.FileUtils;
import com.changer.openmap.boot.admin.utils.PTConsts;
import com.changer.openmap.boot.admin.utils.PTResult;
import com.changer.openmap.boot.admin.utils.PTUtil;
import com.changer.ovep.utils.DateTimeFunc;

import reactor.core.publisher.Mono;

@Service
public class PTCheckService implements ICheckService {
	private final static Logger log = LoggerFactory.getLogger(PTCheckService.class);
	private List<PTCheckConfig> ptConfigs = Collections.synchronizedList(new ArrayList<>());

	//刷新配置中
	private boolean isRefreshConfig = false;
	
	@Autowired
	private WebApplicationContext webApp;
	@Autowired
	private PtAutoConfiguration ptAutoConfiguration;

	@Override
	public boolean checkTelnet(String host, int port) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean checkPing(String host) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@PostConstruct
	public void init() {
		isRefreshConfig = true;
		try {
			ptConfigs.clear();
			File file = new File("config");
			if (!file.exists()){
				return;
			}
			for (File f : file.listFiles()) {
				if (f.isDirectory() && "pt".equalsIgnoreCase(f.getName())) {
					for (File f1 : f.listFiles()) {
						String fileName = f1.getName();
						String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
						if(!"json".equalsIgnoreCase(suffix)) {
							continue;
						}
						String str;
						try {
							str = FileUtils.readToBuffer(f1);
							PTCheckConfig config = DataJsonParser.parseDataInfo(str);
							if (config != null) {
								config.setFileName(f1.getName());
								String host = config.getHost();
								if(host != null && host.contains("{host}")) {
									host = host.replace("{host}", ptAutoConfiguration.getHost());
									config.setHost(host);
								}
								ptConfigs.add(config);
							}
						} catch (IOException e) {
							log.error("init", e);
							log.error("");
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("init", e);
		} finally {
			isRefreshConfig = false;
		}
	}
	
	
	private Date parseStartEndTime(String strTime) {
		Date startTime = null;
		if(strTime != null) { 
			if(strTime.indexOf("-") > 0) {
				startTime = DateTimeFunc.strToDate(strTime.trim());
			}else if(strTime.indexOf(":") > 0) {
				String h = DateTimeFunc.dateToStr(new Date(), "yyyy-MM-dd ") + strTime.trim();
				startTime = DateTimeFunc.strToDate(h, "yyyy-MM-dd HH:mm:ss");
			}
		}
		return startTime;
	}

	private boolean isCheck(PTCheckConfig config) {
		if(config == null) {
			return false;
		}
		if(config.getHost() == null || "".equals(config.getHost().trim())) {
			return false;
		}
		if(PTConsts.CheckType.TELNET.equals(config.getType()) && config.getPort() <= 0) {
			log.warn("telnet port <= 0. data={}", config);
			return false;
		}
		long interval = System.currentTimeMillis() - config.getTempLastTime().getTime();
		if(config.getInterval() > 0 && interval < config.getInterval()) {
			return false;
		}
		Date startTime = parseStartEndTime(config.getStartTime());
		Date endTime = parseStartEndTime(config.getEndTime());
		long c = System.currentTimeMillis();
		if(startTime != null && c < startTime.getTime() ) {
			return false;
		}
		if(endTime != null && c > endTime.getTime()) {
			return false;
		}
		config.setTempRealSendInterval(interval);
		return true;

	}
	
	/**
	 * 发送通知
	 * @param pt
	 */
	protected void sendNotification(OcmInstanceEvent event) {
		if(event == null) {
			return;
		}
		try {
			Map<String, OcmAbstractEventNotifier> mapBean = webApp.getBeansOfType(OcmAbstractEventNotifier.class);
			for(OcmAbstractEventNotifier bean: mapBean.values()) {
				if(!bean.isEnabled()) {
					continue;
				}
				log.debug("OcmAbstractEventNotifier=" + bean);
				bean.notify(event).subscribe(System.out::println, System.err::println);
			}
		} catch (Exception e) {
			log.error("sendNotification(" + event + ")", e);
		}
	}
	
	protected String checkOne(PTCheckConfig pt) {
		if(!isCheck(pt)) {
			return "Not send(isSend=false).";
		}
		try {
			if(isRefreshConfig) {
				return "refresh config.";
			}
			PTResult result = null;
			if(PTConsts.CheckType.PING.equals(pt.getType())) {
				result = PTUtil.pingResult(pt.getHost(), pt.getTimeout());
			}else {
				result = PTUtil.telnetResult(pt.getHost(), pt.getPort(), pt.getTimeout());
			}
			if(result != null) {
				if(PTConsts.CheckStatus.NORMAL.equals(result.getCode())) {
					pt.setTempFailCount(0);
					pt.setLastStatus(pt.getStatus());
					pt.setStatus(result.getCode());
				} else {
					pt.setTempFailCount(pt.getTempFailCount() + 1);
				}
				if(pt.getTempFailCount() >= pt.getFailMaxCount()) {
					pt.setLastStatus(pt.getStatus());
					pt.setStatus(result.getCode());
				}
				if(!pt.getStatus().equals(pt.getLastStatus())) {
					StatusInfo statusInfo = new StatusInfo(pt.getStatus());
					statusInfo.getDetails().put("message", result.getMessage());
					OcmInstanceEvent event = new OcmInstanceEvent(pt, statusInfo);
					sendNotification(event);
				}
			}
			long s = pt.getTempRealSendInterval() / 1000;
			log.debug("ok.interval={}s fileName={} --host={} --port={} --type={} --result={}", 
					s, pt.getFileName(), pt.getHost(), pt.getPort(), pt.getType(), result.toString());
			return "ok.interval=" + s + "s host=" + pt.getHost()  + "  --result=" + result.toString();
		} catch (Exception e) {
			log.error("send", e);
			log.error("host={} --port={} --fileName={}", pt.getHost(), pt.getPort(), pt.getFileName());
			return "error=" + e.getMessage() + "\r\nparams=" + pt; 
		} finally {
			pt.setTempLastTime(new Date());
		}
	}
	
	protected void checkOneAsync(PTCheckConfig pt){
		Mono.fromRunnable(() -> {
			checkOne(pt);
		}).subscribe(System.out::println, System.err::println);
	}

	protected void checkAll() {
		for(PTCheckConfig pt: ptConfigs) {
			if(isRefreshConfig) {
				return;
			}
			checkOneAsync(pt);
		}
	}
	
	
	/**
	 * 定时执行
	 */
	@Scheduled(initialDelay = 1000 * 10, fixedRate = 1000)
	protected void scheduledCheckPT() {
		if(!ptAutoConfiguration.isCheckEnable()) {
			return;
		}
		try {
			checkAll();
		} catch (Exception e) {
			log.error("scheduledCheckPT", e);
		}
	}
	
	

}
