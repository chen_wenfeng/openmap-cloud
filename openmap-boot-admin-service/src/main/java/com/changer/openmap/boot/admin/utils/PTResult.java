package com.changer.openmap.boot.admin.utils;

public class PTResult {
	private PTConsts.CheckStatus code; // 结果编码
	private String message; // 结果描述


	public PTResult() {
		this.setCode(PTConsts.CheckStatus.NORMAL);
		this.setMessage(PTConsts.ResultCode.SUCCESS.msg());
	}

	public PTResult(PTConsts.CheckStatus code) {
		this.setCode(code);
		this.setMessage(code.getDesc());
	}

	public PTResult(PTConsts.CheckStatus code, String message) {
		this.setCode(code);
		this.setMessage(message);
	}

	public PTConsts.CheckStatus getCode() {
		return code;
	}

	public void setCode(PTConsts.CheckStatus code) {
		this.code = code;
		this.message = code.getDesc();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "JsonResult [code=" + code + ", message=" + message + "]";
	}
	
	
}
