package com.changer.openmap.boot.admin.utils;

public class PTConsts {
	/***
	 * 删除状态
	 */
	public static enum CheckStatus {
		NORMAL(0, "NORMAL", "正常"), 
		ABNORMAL(1, "ABNORMAL", "异常"),
		UNKNOWN(2, "UNKNOWN", "未知"),
		OFFLINE(3, "OFFLINE", "离线");
		
		private CheckStatus(int value, String name, String desc) {
			this.value = value;
			this.name = name;
			this.desc = desc;
		}

		private final int value;
		private final String name;
		private final String desc;

		public int getValue() {
			return value;
		}

		public String getName() {
			return name;
		}

		public String getDesc() {
			return desc;
		}
	}

	/***
	 * Result code
	 */
	public static enum ResultCode {
		/** 成功 */
		SUCCESS("200", "成功"), NULL_DATA("205", "无数据"),
		/** 没有登录 */
		NOT_LOGIN("400", "没有登录"),

		/** 发生异常 */
		EXCEPTION("401", "发生异常"),

		/** 系统错误 */
		SYS_ERROR("402", "系统错误"),

		/** 参数错误 */
		PARAMS_ERROR("403", "参数错误 "),

		/** 不支持或已经废弃 */
		NOT_SUPPORTED("410", "不支持或已经废弃"),

		/** AuthCode错误 */
		INVALID_AUTHCODE("444", "无效的AuthCode"),

		/** 太频繁的调用 */
		TOO_FREQUENT("445", "太频繁的调用"),

		/** 未知的错误 */
		UNKNOWN_ERROR("499", "未知错误");

		private ResultCode(String value, String msg) {
			this.val = value;
			this.msg = msg;
		}

		public String val() {
			return val;
		}

		public String msg() {
			return msg;
		}

		private String val;
		private String msg;
	}
	
	/**
	 * 检测类型
	 * <p>Title: PTConstants.java </p>
	 * <p>Description: TODO(这里用一句话描述这个类的作用)</p>
	 * <p>Copyright: Copyright (c) 2019</p>
	 * <p>Company: Changer</p>
	 * @author cwf
	 * @date 2019年8月21日
	 * @version 1.0
	 * 注意：本内容仅限于福州创杰信息科技有限公司内部传阅，禁止外泄以及用于其他的商业目
	 */
	public static enum CheckType {
		PING, TELNET
	}


}