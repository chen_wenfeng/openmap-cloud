package com.changer.openmap.boot.admin.utils;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.changer.openmap.boot.admin.config.PTCheckConfig;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DataJsonParser {
	private static Logger logger = LoggerFactory.getLogger(DataJsonParser.class);
	private static ObjectMapper mapper = null;
	
	public static ObjectMapper getJacksonObjectMapper() {
		if(mapper != null) {
			return mapper;
		}
		mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper;
	}
	
	public static <T> T parse(String strJson, Class<T> cls) {
		if(strJson == null || "".equals(strJson.trim())) {
			return null;
		}
		getJacksonObjectMapper();
		try {
			T info = mapper.readValue(strJson, cls);
			return info;
		} catch (JsonParseException e) {
			logger.error("Json parse error.", e);
		} catch (JsonMappingException e) {
			logger.error("Json parse error.", e);
		} catch (IOException e) {
			logger.error("Json parse error.", e);
		}
		return null;
	}
	
	
	public static PTCheckConfig parseDataInfo(String strJson) {
		if(strJson == null || "".equals(strJson.trim())) {
			return null;
		}
		getJacksonObjectMapper();
		try {
			PTCheckConfig info = mapper.readValue(strJson, PTCheckConfig.class);
//			JsonNode root = mapper.readTree(strJson);
//			JsonNode dataNode = root.get("requestBody");
//			String jsonData = dataNode.toString();
//			logger.info("jsonData=" + jsonData);
//			info.setJsonRequestBody(jsonData);
			return info;
		} catch (JsonParseException e) {
			logger.error("Json parse error.", e);
		} catch (JsonMappingException e) {
			logger.error("Json parse error.", e);
		} catch (IOException e) {
			logger.error("Json parse error.", e);
		}
		return null;
	}
	
	public static String toJSONString(Object obj) {
		getJacksonObjectMapper();
		try {
			return mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			logger.error("To json string error.", e);
		}
		return null;
	}
}
