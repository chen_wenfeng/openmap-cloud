#datename=$(date +%Y%m%d%H%M%S)

echo "setup startup begin"
systemInfo=`cat /etc/redhat-release`
echo "systemInfo=$systemInfo"
systemver=`cat /etc/redhat-release|sed -r 's/.* ([0-9]+)\..*/\1/'`
echo "CenstOS-"$systemver
if [[ $systemver = "7" ]];then
  cp openmap-boot-admin.service /usr/lib/systemd/system/
  echo "copy openmap-boot-admin service complete."
  systemctl daemon-reload
  systemctl enable openmap-boot-admin
  echo "systemctl enable complete."

else

  chmod +x /opt/changer/server/v5/boot-admin/*.jar
  sudo ln -s -f /opt/changer/server/v5/boot-admin/openmap-boot-admin-service-1.0.0.jar /etc/init.d/openmap-boot-admin 
  sudo chkconfig --add openmap-boot-admin
  sudo chkconfig openmap-boot-admin on 
fi

echo "setup startup complete."
